import net.xeoh.plugins.base.annotations.PluginImplementation;
import net.xeoh.plugins.base.annotations.events.Init;
import yourfunworldstudios.socialconquer.Main;
import yourfunworldstudios.socialconquer.DevMechanics.Expansion;

@PluginImplementation
public class Example implements Expansion {
	
	@Init
	public void onEnable() {
		Main.log.info("Enabling SocialConquer Mum's Car extension");
		AbstractCommand myCommand = new MyCommand("mumscar", "/<command> [args]", "This is an example command.");
		myCommand.register();
		Main.log.info("Enabled SocialConquer Mum's Car Extension!");
	}

}
